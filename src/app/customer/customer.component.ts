import {Component, OnInit, ViewChild} from '@angular/core';
import {CustomerService} from './customer.service';
import {LocalDataSource} from 'ng2-smart-table';
import {TransactionService} from '../transaction/transaction.service';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  public transactionList = null;
  public source: LocalDataSource = new LocalDataSource();
  customerTableSetting = {
    actions: {
      add: true,
      edit: false,
      delete: true,
    },
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: true,
    },
    columns: {
      id: {
        title: 'ID No.',
        type: 'string',
        addable: false
      },
      name: {
        title: 'Name',
        type: 'string',
      },
      age: {
        title: 'Age',
        type: 'string',
      }
    }
  };

  constructor(private customerService: CustomerService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.customerService.findAll().subscribe(
      res => {
        this.source.load(res);
      }
    );
  }

  onCreateConfirm(event) {
    if (window.confirm('Are you sure you want to create?')) {
      this.customerService.save(event.newData).subscribe(
        res => {
          event.confirm.resolve(res);
        }
      );
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      this.customerService.delete(event.data.id).subscribe(
        respose => {
          event.confirm.resolve();
        },
      );
    }
  }

  onUserRowSelected(event): void {
    if (event.data) {
      this.transactionList = event.data.transactionList;
    }
  }
}
