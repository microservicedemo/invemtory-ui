import {Transaction} from '../transaction/transaction.model';

export class Customer {
  id: number;
  name: string;
  age: number;
  transactionList: Transaction[];
}
