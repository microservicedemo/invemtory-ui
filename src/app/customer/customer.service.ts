import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Customer} from './customer.model';

@Injectable()
export class CustomerService {
  private resourceUrl = 'http://localhost:8282/cust-man-api/customers';

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(this.resourceUrl);
  }

  save(cus: Customer): Observable<Customer> {
    return this.httpClient.post<Customer>(this.resourceUrl, cus);
  }

  delete(id: number) {
    return this.httpClient.delete(this.resourceUrl + '/' + id);
  }

}
