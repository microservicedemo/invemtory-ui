import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class AccountService {
  private resourceUrl = '/acc-api/accounts';

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<Account[]> {
    return this.httpClient.get<Account[]>(this.resourceUrl);
  }

  save(acc: Account): Observable<Account> {
    return this.httpClient.post<Account>(this.resourceUrl, acc);
  }

  delete(id: number) {
    return this.httpClient.delete(this.resourceUrl + '/' + id);
  }

}
