export class Account {
  id: number;
  accountNumber: string;
  currentBalance: number;
  description: string;
}
