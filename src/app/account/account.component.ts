import { Component, OnInit } from '@angular/core';
import {AccountService} from './account.service';
import {LocalDataSource} from 'ng2-smart-table';
import {Account} from './account.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  public accounts: Account[];
  public source: LocalDataSource = new LocalDataSource();
  accountTableSetting = {
    actions: {
      add: true,
      edit: false,
      delete: true,
    },
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: true,
    },
    columns: {
      id: {
        title: 'ID No.',
        type: 'string',
        addable: false
      },
      accountNumber: {
        title: 'Account Number',
        type: 'string',
      },
      currentBalance: {
        title: 'Current Balance',
        type: 'string',
      },
      description: {
        title: 'Description',
        type: 'string',
      },
    },
  };

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.accountService.findAll().subscribe(
      res => {
        this.source.load(res);
      }
    );
  }

  onCreateConfirm(event) {
    if (window.confirm('Are you sure you want to create?')) {
      this.accountService.save(event.newData).subscribe(
        res => {
          event.confirm.resolve(res);
        }
      );
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      this.accountService.delete(event.data.id).subscribe(
        respose => {
          event.confirm.resolve();
        },
      );
    }
  }

}
