import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountComponent } from './account/account.component';
import { TransactionComponent } from './transaction/transaction.component';
import {NgbButtonsModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {AccountService} from './account/account.service';
import {TransactionService} from './transaction/transaction.service';
import { CustomerComponent } from './customer/customer.component';
import {CustomerService} from './customer/customer.service';
@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    TransactionComponent,
    CustomerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgbButtonsModule,
    Ng2SmartTableModule
  ],
  providers: [
    AccountService,
    TransactionService,
    CustomerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
