import { Component, OnInit } from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table';
import {TransactionService} from './transaction.service';
import {CustomerService} from '../customer/customer.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  public source: LocalDataSource = new LocalDataSource();
  transactionTableSetting = {
    actions: {
      add: true,
      edit: false,
      delete: true,
    },
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: true,
    },
    columns: {
      id: {
        title: 'ID No.',
        type: 'string',
        addable: false
      },
      description: {
        title: 'Description',
        type: 'string',
      },
      amount: {
        title: 'Amount (Rs.)',
        type: 'string',
      },
      customerId: {
        title: 'Customer ID',
        editor: {
          type: 'list',
          config: {
            selectText: 'Select',
            list: [],
          },
        },
      },
    }
  };

  constructor(private transactionService: TransactionService,
              private customerService: CustomerService) { }

  ngOnInit() {
    this.customerService.findAll().subscribe(
      custres => {
        const customerList = [];
        custres.forEach(cust => {
          const custItem: any = {};
          custItem.value = cust.id;
          custItem.title = cust.name;
          customerList.push(custItem);
        });
        this.transactionTableSetting.columns.customerId.editor.config.list = customerList;
        this.transactionTableSetting = Object.assign({}, this.transactionTableSetting);
        this.transactionService.findAll().subscribe(
          res => {
            this.source.load(res);
          }
        );
      }
    );

  }

  onCreateConfirm(event) {
    if (window.confirm('Are you sure you want to create?')) {
      this.transactionService.save(event.newData).subscribe(
        res => {
          event.confirm.resolve(res);
        }
      );
    } else {
      event.confirm.reject();
    }
  }

  onDeleteConfirm(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      this.transactionService.delete(event.data.id).subscribe(
        respose => {
          event.confirm.resolve();
        },
      );
    }
  }
}
