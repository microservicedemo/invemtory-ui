import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Transaction} from './transaction.model';

@Injectable()
export class TransactionService {
  private resourceUrl = 'http://localhost:8181/acc-api/transactions';

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<Transaction[]> {
    return this.httpClient.get<Transaction[]>(this.resourceUrl);
  }

  save(acc: Transaction): Observable<Transaction> {
    return this.httpClient.post<Transaction>(this.resourceUrl, acc);
  }

  delete(id: number) {
    return this.httpClient.delete(this.resourceUrl + '/' + id);
  }
}
