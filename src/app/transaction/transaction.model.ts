export class Transaction {
  id: number;
  description: string;
  amount: number;
  customerId: string;
}
